class Persona:
    def __init__(self, nombre, cedula):
        self.__nombre = nombre
        self.__cedula = cedula
        self.__listaActividad = {}

    def getNombre(self):
        return self.__nombre

    def setNombre(self, nombre):
        self.__nombre = nombre

    def getCedula(self):
        return self.__cedula

    def setCedula(self, cedula):
        self.__cedula = cedula

    def agregarActividad(self, actividad):
        self.__listaActividad[actividad.getCodigo()] = actividad

    def getListaActividad(self):
        return self.__listaActividad