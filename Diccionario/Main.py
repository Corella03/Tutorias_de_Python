from Persona import *
from Actividad import *
listaPersona = {}


def imprimirPersona():
    for clave, valor in listaPersona.items():
        persona = valor
        print("Nombre: ", persona.getNombre(), "\nCedula: ",persona.getCedula())

def agregarPersona(nombre, cedula):
    nuevaPersona = Persona(nombre,cedula)
    listaPersona[cedula] = nuevaPersona


def verificarPersona(cedula):
    for clave, valor in listaPersona.items():
        if clave == cedula:
            return True
    return False

def agregarActividad(cedula, nomActividad, codigo):
    actividad = Actividad(nomActividad, codigo)
    for clave, valor in listaPersona.items():
        if clave == cedula:
            persona = valor
            persona.agregarActividad(actividad)
            listaPersona[cedula] = persona

def imprimirActividad(cedula):
    for clave, valor in listaPersona.items():
        persona = valor
        if clave == cedula:
            for key, value in persona.getListaActividad().items():
                actividad = value
                print("Nombre: ", actividad.getNombre(), "\nCodigo: ", actividad.getCodigo())

def menu():
    print(" 1) Agregar Persona.\n", "2) Agregar Actividad.\n", "3) Mostrar.\n")
    try:
        opcion = int(input("Seleccione una opción: "))
        if opcion == 1:
            nombre = input("Ingrese el nombre de la persona: ")
            cedula = int(input("Ingrese la cedula de la persona: "))
            agregarPersona(nombre, cedula)
            return menu()
        elif opcion == 2:
            cedula = int(input("Ingrese la cedula de la persona: "))
            if(verificarPersona(cedula)):
                nomActividad = input("Ingrese el nombre de la actividad: ")
                codigo = input("Ingrese el codigo de la actividad: ")
                agregarActividad(cedula, nomActividad, codigo)
                imprimirActividad()
            return menu()
        elif opcion == 3:
            cedula = int(input("Ingrese la cedula de la persona: "))
            imprimirActividad(cedula)
    except:
        print("Ocurrio ")

menu()